class WebApi::InvestorsController < WebApi::ApplicationController
  respond_to :json

  def show
    @investor = Investor.find(params[:id])
    respond_with @investor, include: :borrowers, location: nil
  end

end
