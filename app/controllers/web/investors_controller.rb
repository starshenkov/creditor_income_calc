class Web::InvestorsController < ApplicationController

  def index
    @investors = Investor.all
  end

  def new
    @investor = Investor.new
  end

  def create
    @investor = Investor.new(investor_params)
    if @investor.save
      f(:success)
      redirect_to investors_path
    else
      f(:error)
      render :new
    end
  end

  def edit
    @investor = Investor.find(params[:id])
  end

  def update
    @investor = Investor.find(params[:id])

    if @investor.update investor_params
      f(:success)
      redirect_to investors_path
    else
      f(:error)
      render :edit
    end
  end

  def investor_params
    params.require(:investor).permit(:amount, :period, :rate, :expire_rate)
  end

end
