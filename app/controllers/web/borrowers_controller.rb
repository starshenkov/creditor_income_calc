class Web::BorrowersController < ApplicationController

  def index
    @borrowers = Borrower.all
  end

  def new
    @borrower = Borrower.new
  end

  def create
    @borrower = Borrower.new(borrower_params)
    if @borrower.save
      f(:success)
      redirect_to borrowers_path
    else
      f(:error)
      render :new
    end
  end

  def edit
    @borrower = Borrower.find(params[:id])
  end

  def update
    @borrower = Borrower.find(params[:id])

    if @borrower.update borrower_params
      f(:success)
      redirect_to borrowers_path
    else
      f(:error)
      render :edit
    end
  end

  def borrower_params
    params.require(:borrower).permit(:investor_id, :payments)
  end

end
