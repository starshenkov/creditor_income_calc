Rails.application.routes.draw do

  root to: "web/investors#index"
  scope module: "web" do
    resources :investors, only: [:index, :new, :create, :edit, :update]
    resources :borrowers, only: [:index, :new, :create]
  end

  namespace :web_api, format: :json do
    resources :investors, only: [:show]
  end

end
