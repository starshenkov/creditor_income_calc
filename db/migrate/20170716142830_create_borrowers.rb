class CreateBorrowers < ActiveRecord::Migration
  def change
    enable_extension "hstore"
    create_table :borrowers do |t|
      t.string :payments
    end
  end
end
