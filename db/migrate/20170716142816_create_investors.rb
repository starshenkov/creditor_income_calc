class CreateInvestors < ActiveRecord::Migration
  def change
    create_table :investors do |t|
      t.integer :amount
      t.integer :period
      t.integer :rate
      t.integer :expire_rate

      t.timestamps null: false
    end
  end
end
