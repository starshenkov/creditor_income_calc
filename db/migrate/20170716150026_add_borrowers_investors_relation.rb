class AddBorrowersInvestorsRelation < ActiveRecord::Migration

  def change
    add_reference :borrowers, :investor, index: true
  end

end
